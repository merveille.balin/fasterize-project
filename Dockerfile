FROM node:16-alpine

WORKDIR /usr/app 

COPY package*.json ./ 

COPY .env /.env

COPY . . 

RUN npm install 

EXPOSE 3000 

CMD ["npm", "start"]