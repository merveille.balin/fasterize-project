const Sequelize = require('sequelize')


const dbConfig = {
    username : 'test',
    password: 'azertyuiop',
    database : 'fasterize',
    host : '127.0.0.1',
    port : 3306,
    dialect : "mariadb"
  }
  
const sequelize = new Sequelize(dbConfig.database, dbConfig.username, dbConfig.password, dbConfig)


sequelize.define('image', {

        //Id
        id : {
            type : Sequelize.UUID, 
            primaryKey : true,
            allowNull : false
        },

        //Date of purchase
        style : {
            type : Sequelize.JSON,
            //allowNull : false
        },

        //Count of invitations
        file : {
            type : Sequelize.BLOB,
            //allowNull : false
        },

        //Global price for the pack
        quality : {
            type : Sequelize.INTEGER,
            allowNull : false,
            defaultValue : 100
        },

        optimaze : {
           type : Sequelize.BOOLEAN,
           allowNull : false,
           defaultValue : false
        } 

    })



sequelize.sync({force: true}).catch((err) => {
    console.log(err)
})

module.exports = sequelize