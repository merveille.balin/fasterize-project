const express = require('express')
const _image = require('../controllers/image.controllers')
const router  = express.Router()


router.get('/', (req,res) => {
    _image.getInfo(req.query.id).then((image)=> {
        if (image){
            res.send(image)
        }else{
            res.status(400).send({ message : "image does not exit" })
        }
    })
});

router.post('/edit', (req, res) => {
    res.sendStatus(200)
})

router.post('/upload', (req, res) => {
    res.sendStatus(200)
})

module.exports = router