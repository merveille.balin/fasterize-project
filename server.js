const express = require('express')
const app = express()
const PORT = 3000

app.use(express.json())

app.use('/api/image', require('./api/routes/image.routes'))

console.log('test')

//listen port on 3000
app.listen(PORT, () => {
    //console.log('test')
    console.log(`server run on port ${PORT}`)  
})