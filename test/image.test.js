const mocha  = require('mocha')
const express = require('express')
const axios = require('axios').default
const expect = require('chai').expect
const uuid = require('uuid') 
const fs = require('fs')

const URL = 'http://localhost:3000'
const IMAGEID = ''

try {
    const image = fs.readFileSync('./test/files/mariadbpullimage.jpeg')
    const UPLOADFILE = image
    
    console.log(UPLOADFILE)
 
} catch (err) {
    console.error(err)
}

describe('test endpoint image', () => {
    it(`image doesn't exit`, (done) => {
        axios.get(`${URL}/api/image?id=${IMAGEID}`).catch((err)  => {
            expect(err.response.status).to.equal(400)
            done()
        })
    })

    it(`upload images`, (done) => {
        axios.post(`${URL}/api/image`, UPLOADPARMS).then((res) => {
            expect(res.status).to.equal(200) 
            expect(res.data.id).to.exist
            IMAGEID = res.data.id
            done()
        })
    })

    it(`test the case if the image is well uplaoded`, (done) => {
        axios.post(`${URL}/api/image?id=${IMAGEID}`).then( (res) => {
            expect(res.data.id).to.equal(IMAGEID)
            expect(res.data.style).to.not.be.null
            expect(res.data.quality).to.equal(100)
            expect(res.data.optmize).to.be.false
            expect(res.data.src).to.exist
            done()
        })
    })

    it(`test the image modification case`, (done) => {
        axios.post(`${URL}/api/image`).then( (res) => {
            done()
        })
    })

    it(`verify that the image attributes is well updated or modified`, (done) => {
        axios.get(`${URL}/api/image?id=${IMAGEID}`).catch((err)  => {
            expect(err.response.status).to.equal(400)
            done()
        })
    })
})


